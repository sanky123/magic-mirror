var player;
var token;
var deviceId;

$.getJSON("authorizationToken.json",function(data){
   var obj = data
   token = obj["accessToken"];
})

window.onSpotifyWebPlaybackSDKReady = function () {
  //get authentication token.
  //token = 'BQCy0dOFrGsBy6qJowf-A7RY5tyljMI6tNWI6fJEq8Eu293_7mf3rscJC_vJb-MeScQEqJcSgpb_yRoDTp49wn6ZsGQF1V9OSioou_p_oh9tXQsPQftOofo4txvu6jFflhLXckj1Nda6QeX3D6XXFkcltubYuj-16jY';


  player = new Spotify.Player({
    name: 'Magic Mirror Player',
    getOAuthToken: function(callback){callback(token); },
    volume: 1,
  });

  // Error handling
  player.addListener('initialization_error', function(message){ console.error(message); });
  player.addListener('authentication_error', function(message){
    console.log(message.message);
    if(message.message == "Authentication failed"){
      window.location.replace("http://localhost:3000/refresh");
    }
  });
  player.addListener('account_error', function(message){ console.error(message); });

  player.addListener('playback_error', function(message){
    console.error(message.message);
    if(message.message == "Cannot perform operation; no list was loaded."){
      startingMusic();
    }
  });

  // Playback status updates
  player.addListener('player_state_changed', function(playerState){ console.log(playerState); });

  // Ready
  player.addListener('ready', function(device_id) {
    deviceId = device_id.device_id;
    console.log('Ready with Device ID and to play music', device_id);
  });

  // Not Ready
  player.addListener('not_ready', ({ device_id }) => {
    console.log('Device ID has gone offline', device_id);
  });

  // Connect to the player!
  player.connect();

};


function startingMusic(){
  //turnOnShuffle();
  $.ajax({type:"PUT",
          url:"https://api.spotify.com/v1/me/player/play?device_id=" +deviceId,
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
          },
          cache: false,
          data: "{\"context_uri\":\"spotify:user:sankalp2:playlist:5AiYCHK2enjzXVbobRXt5u\"}",
          success:function(data){console.log("no error")},
          error:function(data){console.log(data)}
        })
}

function turnOnShuffle(){
  $.ajax({type:"PUT",
          url:"https://api.spotify.com/v1/me/player/shuffle?state=true&device_id=" + deviceId,
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
          },
          cache: false,
          data: "{\"context_uri\":\"spotify:user:sankalp2:playlist:5AiYCHK2enjzXVbobRXt5u\"}",
          success:function(data){console.log("no error")},
          error:function(data){console.log(data)}
  })
}

function skipSong(){
  $.ajax({type:"POST",
          url:"https://api.spotify.com/v1/me/player/next?device_id="+deviceId,
          headers:{
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
          },
          cache: false,
          success:function(data){console.log("no error")},
          error:function(data){console.log(data)}
        })
}





var displayCounter = 0;
var compliments = ["You look particularly fantastic today.",
                   "You dress really well.",
                   "Whoa, your outfit is so amazing!",
                   "I love your sense of style.",
                   "You always look so cute whenever I see you!",
                   "Your energy is infectious.",
                   "You are looking hella dope!",
                   "Hey there handsome!",
                   "Wow you look so good!",
                   "Go and conquer the day!"];

function load(){
  getTime();
  var min = 60 * 1000
  $.ajax({url:"https://api.darksky.net/forecast/bdc30fa966ceae7143bda57166d6333e/41.5772,-93.7113"})
      .done(weather);
  setInterval(getTime,1000);
  setInterval(function(){
    $.ajax({url:"https://api.darksky.net/forecast/bdc30fa966ceae7143bda57166d6333e/41.5772,-93.7113"})
      .done(weather);
    },min * 30)

}

function getTime(){
  var time = new Date();
  document.getElementById('Time').innerHTML = time.toLocaleTimeString();
  document.getElementById('Date').innerHTML = time.toString().substring(0,15);
  displayCounter++;

  if((displayCounter % 15 == 0) && (document.getElementById('voiceToText').innerHTML != "")){
    document.getElementById('voiceToText').innerHTML = ""
  }
}

function weather(info){
  var skycons = new Skycons({"color": "white"});
  var elementString;
  var day;
  var weather;

  document.getElementById('todayWeather').innerHTML =
            Math.ceil(info.currently.temperature) + "&deg;F"

  document.getElementById("todayHigh_Low").innerHTML =
            Math.ceil(info.daily.data[0].temperatureHigh) + "&deg;F" + "&emsp;&emsp;" +
            Math.floor(info.daily.data[0].temperatureLow) + "&deg;F"


  skycons.add("today", getIcon(info.daily.data[0].icon))

  for(var x = 1; x < 5; x++){
    elementString = "day" + (x+1)
    day = elementString + "Day"
    weather = elementString + "Weather"
    document.getElementById(day).innerHTML =
                (new Date(info.daily.data[x].time * 1000)).toString().substring(0,4);
    document.getElementById(weather).innerHTML =
            Math.ceil(info.daily.data[x].temperatureHigh) + "&deg;F" + "&emsp;&emsp;&emsp;" +
            Math.floor(info.daily.data[x].temperatureLow) + "&deg;F"
    setIcon(elementString,info.daily.data[x].icon,skycons)
  }
  skycons.play();

}

function setIcon(element,iconType, sky){
    elementID = element.charAt(0).toUpperCase() + element.substring(1);
    var icon = getIcon(iconType)
    sky.add(elementID,icon);
}

function getIcon(icon){
  switch (icon){
    case "rain":
      return Skycons.RAIN
      break;
    case "clear-night":
      return Skycons.CLEAR_NIGHT
      break;
    case "partly-cloudy-day":
      return Skycons.PARTLY_CLOUDY_DAY
      break;
    case "partly-cloudy-night":
      return Skycons.PARTLY_CLOUDY_NIGHT
      break;
    case "cloudy":
      return Skycons.CLOUDY
      break;
    case "sleet":
      return Skycons.SLEET
      break;
    case "snow":
      return Skycons.SNOW
      break;
    case "wind":
      return Skycons.WIND
      break;
    case "fog":
      return Skycons.FOG
      break;
    default:
      return Skycons.CLEAR_DAY
      break;
  }
}

if(annyang){
  var commands = {"mirror mirror on the wall *tag":  test,
                  "mirror *tag": test,
                  "hello *tag": test,
                  "hi *tag": test,
                  "spotify *tag": spotify}

  annyang.addCommands(commands);
  annyang.addCallback('error', function() {
         console.log('There was an error in Annyang!');
  });

  annyang.start({autoRestart:true,continous:false});
}

function test(tag){
  displayCounter = 0;
  if(tag == "who's the flyest of them all"){
    document.getElementById('voiceToText').innerHTML = "You are the flyest of them all!"
  }else{

    document.getElementById("voiceToText").innerHTML = compliments[Math.floor(Math.random() * 10)]
  }
}

function spotify(tag){
  if(tag.toUpperCase() == "PAUSE"){
    player.pause();
  }else if (tag.toUpperCase() == "RESUME" || tag.toUpperCase() == "PLAY") {
    player.resume();
  }else if (tag.toUpperCase() == "SHUFFLE"){
    turnOnShuffle();
  }else if (tag.toUpperCase() == "SKIP" || tag.toUpperCase() == "NEXT"){
    skipSong();
  }
}
  //setTimeout(test(),1000)
  //console.log("after 15sec")
  //document.getElementById("voiceToText").innerHTML = "";
