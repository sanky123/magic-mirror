
var express = require('express');
var app = express();

// setup ports
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

app.get('/', function(req, res) {

	res.end('Hello YouTube!');

});

// server listens in on port
app.listen(server_port, server_ip_address, function () {
	 console.log( "Listening on " + server_ip_address + ", server_port " + server_port );
});




// var express = require('express');
// //var http = require('http');
// var fs = require('fs');
// var request = require('request');
// var app = express();
//
// Object.assign=require('object-assign');
//
// app.use(express.static('public'))
//
// var serverPort = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
// var server_ip_address = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
//
// app.get('/', function (req, res) {
//   res.end("HELLO");
//   var clientId = "12658ae9b9064f0db08318732182a74d";
//   //var redirectUri = "http://localhost:3000/callback"
//   var scopes = "user-library-read playlist-read-private user-library-modify playlist-modify-public user-read-private playlist-modify-private user-read-recently-played user-read-email streaming user-top-read playlist-read-collaborative user-modify-playback-state user-follow-modify user-follow-read user-read-currently-playing user-read-playback-state"
//   res.redirect("https://accounts.spotify.com/authorize/?client_id="+clientId+"&response_type=code&redirect_uri="+encodeURIComponent(redirectUri)+"&scope="+ encodeURIComponent(scopes));
// });
//
//
//
// app.get("/callback",function(req,res){
//   //console.log(req.query.code)
//   var clientId = "12658ae9b9064f0db08318732182a74d";
//   var clientSecret = "6e7151fc00a541aa8a8f8214afb5261b"
//   var redirectUri = "http://localhost:3000/callback"
//
//   var authData = {
//       url: 'https://accounts.spotify.com/api/token',
//       form: {
//         code: req.query.code,
//         redirect_uri: redirectUri,
//         grant_type: 'authorization_code'
//       },
//       headers: {
//         'Authorization': 'Basic ' + (new Buffer(clientId + ':' + clientSecret).toString('base64'))
//       },
//       json: true
//     }
//
//   request.post(authData, function(error, response, body) {
//       var content;
//       var accessToken = body.access_token
//       var refreshToken = body.refresh_token;
//       //console.log("AccessToken");
//       //console.log(accessToken);
//       //console.log("RefreshToken")
//       //console.log(refreshToken);
//       content = {"accessToken" : accessToken,
//                  "refreshToken" : refreshToken
//                 }
//       content = JSON.stringify(content);
//
//       fs.writeFileSync("public/authorizationToken.json",content,'utf8');
//       res.redirect('/MagicMirror');
//       //res.sendfile('public/MagicMirror.html',{ root: __dirname + '/Mirror' });
//   })
// });
//
//
//
// app.get("/refresh",function(req,res){
//   //console.log("/refresh")
//   var clientId = "12658ae9b9064f0db08318732182a74d";
//   var clientSecret = "6e7151fc00a541aa8a8f8214afb5261b"
//   var redirectUri = "http://localhost:3000/callback"
//   var obj = JSON.parse(fs.readFileSync('public/authorizationToken.json', 'utf8'))
//   var oldRefreshToken = obj.refreshToken;
//   //console.log(oldRefreshToken)
//   var authData = {
//       url: 'https://accounts.spotify.com/api/token',
//       form: {
//         grant_type: 'refresh_token',
//         refresh_token: oldRefreshToken
//       },
//       headers: {
//         'Authorization': 'Basic ' + (new Buffer(clientId + ':' + clientSecret).toString('base64'))
//       },
//       json: true
//     }
//
//   //console.log(authData);
//
//   request.post(authData, function(error, response, body) {
//        if(error){
//         console.log(error);
//       }
//       //console.log(response);
//        var content;
//        var accessToken = body.access_token
//        var refreshToken = body.refresh_token;
//   //
//        content = {"accessToken" : accessToken,
//                   "refreshToken" : refreshToken
//                  }
//        content = JSON.stringify(content);
//        fs.writeFileSync("public/authorizationToken.json",content,'utf8');
//
//       res.redirect('/MagicMirror');
//   })
// });
//
//
//
// app.get("/MagicMirror",function(req,res){
//   res.sendfile('public/MagicMirror.html');
// })
//
//
//
// app.listen(serverPort,server_ip_address, function(){
//   console.log("App listening on Port" + serverPort + "Server IP" + server_ip_address);
// })
